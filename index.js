
const express = require("express");

const mongoose = require("mongoose");

const app = express();

const port = 3001;

mongoose.connect("mongodb+srv://orlaguisando09:Cloudorl09@orl.cxxn3.mongodb.net/batch164_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	} 

);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))




app.use(express.json());

app.use(express.urlencoded({ extended:true}));


const userSchema = new mongoose.Schema({
	
	userName: String,
	password: String,
	status: {
		type: String,

		default: "pending"
	}
})

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
	User.findOne({ userName: req.body.userName	}, (err, result) => {
		if (result != null && result.userName == req.body.name) {
			return res.send("Already registered");
		} else {
			let newUser = new User({
				userName: req.body.userName,
				password: req.body.password
			});

			newUser.save((saveErr, savedTask) => {
				if (saveErr) {
					return console.error(saveErr);
				} else {
					return res.status(201).send(`${req.body.userName} has been created.`);
				}
			});
		}
	})
})

app.get("/users", (req, res) => {
	User.find({	 }, (err, result) => {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				Registered_Accounts: result
			})
		}
	})
})


app.listen(port, () => console.log(`Server running at port ${port}`))
